# pass at build-time to the builder with the docker build command using the --build-arg <varname>=<value> flag.
# some higher versions cause error at jmeter start (checking java version fails)
ARG tag=8-alpine
ARG baseimage=openjdk

FROM ${baseimage}:${tag}
# use in DB AG instead
# FROM default-docker-3rdparty.bahnhub.tech.rz.db.de/${baseimage}:${tag}

# pass to set JMeter versions (default is 2.13), from v.5.0+ switch checksum_type to sha512 passing --build-arg, see below
ARG jmeter_version
ENV JMETER_VERSION ${jmeter_version:-2.13}
# pass to switch checksum type to sha512 (default md5), availability based on JMeter version, see https://archive.apache.org/dist/jmeter/binaries/
ARG checksum_type=md5

LABEL maintainer cops@deutschebahn.com
LABEL app apache-jmeter
LABEL app.version  ${JMETER_VERSION}

# JVM configuration
# supported from JDK 8u131+, see https://very-serio.us/2017/12/05/running-jvms-in-kubernetes/
# for RiM LuP set memory request and limit to 1536M in Deployment, then set JVM_ARGS to '-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap'
ENV JVM_ARGS ''

# JMeter configuration
ENV JMETER_NAME apache-jmeter-${JMETER_VERSION}
ENV JMETER_ARCH ${JMETER_NAME}.tgz
ENV JMETER_HOME /app/${JMETER_NAME}
ENV PATH ${JMETER_HOME}/bin:${PATH}
# from JMEter v3.0+ generates report after test when set to: '-e -o <Path to output folder>'
# see https://jmeter.apache.org/usermanual/generating-dashboard.html#report_after_load_test
ENV REPORT_PARAM ''
# see jmeter-server documentation for RMI_HOST_DEF '-Djava.rmi.server.hostname='
ENV RMI_HOST_DEF ''
# RMI port to be used by the server, the rmiregistry application listens to port 1099 by default
ENV SERVER_PORT 1099
ENV LOCAL_PORT 50000
# Remotes configuration for master (if using master <-> slaves configuration)
ENV REMOTE_HOSTS ''

# Test configuration
# pass this argument (without .jmx) to copy sample-test file (defaults to github.com-test)
ARG test_name
ENV TEST_NAME ${test_name:-openShiftTest}
# use to set path to test file & test config files if needed   
ENV TEST_CONFIG_FOLDER ${JMETER_HOME}/bin

USER root

WORKDIR ${JMETER_HOME}/bin

# copy sample dbag-test.jmx (adapted for DB AG), see https://gist.github.com/jmoberly/ae3e83b3540003734e66
COPY ${TEST_NAME}.jmx ${TEST_CONFIG_FOLDER}/

WORKDIR /app

# download Apache JMeter from https://archive.apache.org/dist/jmeter/binaries/
ADD https://archive.apache.org/dist/jmeter/binaries/${JMETER_ARCH} .
ADD https://archive.apache.org/dist/jmeter/binaries/${JMETER_ARCH}.${checksum_type} .

# verify download, see http://www.apache.org/info/verification.html
RUN ${checksum_type}sum -c ${JMETER_ARCH}.${checksum_type} && \
    # install
    tar -xf ${JMETER_ARCH} && \
    # clean-up
    rm ${JMETER_ARCH} && \
    # change owner and group
    chown -R 98:99 ${JMETER_HOME} && \
    # Support Arbitrary User IDs as recommended by official (OpenShift) documentation, see https://docs.openshift.com/container-platform/3.9/creating_images/guidelines.html
    chgrp -R 0 ${JMETER_HOME} && chmod -R g=u ${JMETER_HOME}

WORKDIR ${JMETER_HOME}/bin

# Expose the ports if and as needed
EXPOSE ${SERVER_PORT} ${LOCAL_PORT}

# switch from root
USER 98

# default entrypoint starts CLI mode sample test and exits, pass your own test file using ENV
ENTRYPOINT [ "sh" ]
# HOSTNAME env will be assigned from cluster
CMD ["-c", "${JMETER_HOME}/bin/jmeter -n \
      -t ${TEST_CONFIG_FOLDER}/${TEST_NAME}.jmx \
      -l ${TEST_CONFIG_FOLDER}/${TEST_NAME}_${HOSTNAME}.jtl \
      -j ${TEST_CONFIG_FOLDER}/${TEST_NAME}_${HOSTNAME}.log \
      ${REPORT_PARAM}"]