# Dockerized jmeter
jmeter in docker, based on openJDK

# ARG baseimage=<value> ARG tag=<value>
 defaults to openjdk:8-alpine for openJDK base image FROM baseimage:tag 
 Pass at build-time to the builder with the docker build command using --build-arg baseimage=<value> --build-arg tag=<value>

# ARG jmeter_version=<value>
defaults to 2.13
Pass to set which JMeter version to install (at build-time to the builder with the docker build command using --build-arg jmeter_version=<value>

# ARG checksum_type=md5
defaults to md5
Pass to switch checksum type to sha512, since JMeter ver.5.0 there is no md5 checksum file, so you have to switch to sha521 using --build-arg checksum_type=sha521 
Checksum availability based on JMeter version, see https://archive.apache.org/dist/jmeter/binaries/

# ENV JVM_ARGS 
set to '-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap' if deploying to k8s and have the memory request and limit set for JVM to obey cgroups (since Java 8u131)

# some other ENVs 
will be needed if you want to deploy master - slave(s) distributed testeing configuration

# Entrypoint 
default runs the sample test in CLI Mode (NON GUi mode) and exits, override with --entrypoint sh at runtime, if you want to peek into the container